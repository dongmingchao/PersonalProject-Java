package com.dedsec.wordCount;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

public class Line {
    private String target;

    public Line(String aLine) {
        target = aLine;
    }

    public long charCount() {
        long res = 0;
        Scanner in = new Scanner(target);
        while (in.hasNext()) {
            res++;
            in.next();
        }
        return res;
    }

    public HashMap getDict(){
        HashMap<String,Long> dict = new HashMap<>();
        Scanner in = new Scanner(target);
        while (in.hasNext()){
            String word = in.next();
            dict.putIfAbsent(word, (long) 0);
            dict.compute(word,(key,value) -> value+1);
        }
        return dict;
    }
}
